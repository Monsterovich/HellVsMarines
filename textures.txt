// Damage Digits ====================================
Sprite D1N0A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM0, 24, 0
}

Sprite D1N1A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM1, 24, 0
}

Sprite D1N2A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM2, 24, 0
}

Sprite D1N3A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM3, 24, 0
}

Sprite D1N4A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM4, 24, 0
}

Sprite D1N5A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM5, 24, 0
}

Sprite D1N6A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM6, 24, 0
}

Sprite D1N7A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM7, 24, 0
}

Sprite D1N8A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM8, 24, 0
}

Sprite D1N9A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM9, 24, 0
}

// ----

Sprite D2N0A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM0, 16, 0
}

Sprite D2N1A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM1, 16, 0
}

Sprite D2N2A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM2, 16, 0
}

Sprite D2N3A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM3, 16, 0
}

Sprite D2N4A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM4, 16, 0
}

Sprite D2N5A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM5, 16, 0
}

Sprite D2N6A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM6, 16, 0
}

Sprite D2N7A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM7, 16, 0
}

Sprite D2N8A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM8, 16, 0
}

Sprite D2N9A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM9, 16, 0
}

//----

Sprite D3N0A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM0, 8, 0
}

Sprite D3N1A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM1, 8, 0
}

Sprite D3N2A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM2, 8, 0
}

Sprite D3N3A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM3, 8, 0
}

Sprite D3N4A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM4, 8, 0
}

Sprite D3N5A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM5, 8, 0
}

Sprite D3N6A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM6, 8, 0
}

Sprite D3N7A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM7, 8, 0
}

Sprite D3N8A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM8, 8, 0
}

Sprite D3N9A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM9, 8, 0
}

//----

Sprite D4N0A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM0, 0, 0
}

Sprite D4N1A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM1, 0, 0
}

Sprite D4N2A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM2, 0, 0
}

Sprite D4N3A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM3, 0, 0
}

Sprite D4N4A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM4, 0, 0
}

Sprite D4N5A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM5, 0, 0
}

Sprite D4N6A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM6, 0, 0
}

Sprite D4N7A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM7, 0, 0
}

Sprite D4N8A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM8, 0, 0
}

Sprite D4N9A0, 32, 32
{
	Offset 16, 16
	Patch DAMGNUM9, 0, 0
}

// M_DOOM pic
graphic M_DOOM, 483, 158
{
Xscale 3.0
Yscale 3.0
Offset 40, -60
Patch M_DOOM, 0, 0
}

Graphic "CURSA0", 256, 256
{
	Patch "CURSA0", 0, 0
	XScale 20
	YScale 20
}
