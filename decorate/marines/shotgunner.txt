Actor ShotgunnerClass : MarineHumanoidClass
{
	Player.ColorRange 112, 127
	Player.SoundClass "Shotgunner"
	Player.DisplayName "Shotgunner"
	Player.CrouchSprite "PLYC"
  
	Player.StartItem "Marine Shotgun"
	Player.StartItem "Marine Pistol"
	Player.StartItem "Marine Fists"
	Player.StartItem "ShotgunShells", 30
	Player.StartItem "PistolClip", 80
	Player.StartItem "WeakBarrelItem", 20
	
	Player.MaxHealth 100

	States
	{
	Spawn:
		PLAY AB 10
		Loop
	See:
		PLAY ABCD 4 
		Loop
	Missile:
		PLAY E 12 
		Goto Spawn
	Melee:
		PLAY F 6 Bright
		Goto Missile
	Pain:
		PLAY G 4 
		PLAY G 4 A_Pain
		Goto Spawn
	Death:
		NULL A 0 A_Jump(100, "Death2", "Death3")
		NULL A 0 A_Jump(45, "WoundDeath")
	Death1:
		PLAY H 10
		NULL A 0 A_SpawnItem("NashGore_GibGenerator", 0, 0, 0, 0)
		PLAY I 10 A_PlayerScream
		PLAY J 10 A_NoBlocking
		PLAY KLM 10
		PLAY N -1
		Stop
	Death2:
		NULL A 0 A_SpawnItem("NashGore_GibGenerator", 0, 0, 0, 0)
		MALT A 4 A_PlayerScream
		MALT BCD 5 A_NoBlocking
		MALT E -1
		Stop
	Death3:
		PLAY H 10
		NULL A 0 A_SpawnItem("NashGore_GibGenerator", 0, 0, 0, 0)
		PLAY I 10 A_PlayerScream
		PLAY J 10 A_NoBlocking
		PLAY K 10
		MHED AB 10
		MHED C -1
		Stop
	WoundDeath:
		MRND A 6 A_PlayerScream
		NULL A 0 A_SpawnDebris("NashGore_FlyingBlood", 1)
		NULL A 0 A_SpawnDebris("NashGore_FlyingBlood", 1)
		NULL A 0 A_NoBlocking
		MRND BCBCBC 10 A_SpawnDebris("NashGore_FlyingBlood", 1)
		MRND DEF 7
		MRND G -1
		Stop
	XDeath:
		PLAY O 5
		PLAY P 5 A_XScream
		PLAY Q 5 A_NoBlocking
		NULL AAAAAA 0 A_SpawnItem("NashGore_GibGenerator", 0, 0, 0, 0)
		PLAY RSTUV 5
		PLAY W -1
		Stop
  }
}

Actor ShotgunShells : Ammo
{
	+INVENTORY.IGNORESKILL
	Inventory.MaxAmount 30
	Inventory.Icon "SHELA0"
}

Actor PistolClip : Ammo
{
	+INVENTORY.IGNORESKILL
	Inventory.MaxAmount 160
	Inventory.Icon "CLIPA0"
}

Actor "Marine Shotgun" : Weapon
{
	Weapon.SlotNumber 3
	Obituary "%o's head got splattered into pieces by %k's shotgun."

	Weapon.AmmoType "ShotgunShells"
	Weapon.AmmoUse 1
	
	Decal "Bullet"
	
	States
	{
	Ready:
		SHTX GK 1
		NULL A 0 A_PlaySound("mshotgun/pump", CHAN_WEAPON)
		SHTX LIECA 1
		Goto ShotgunReady
	ShotgunReady:
		NULL A 0 A_JumpIfInventory("BotAction1", 1, "Fire")
		SHTX A 1 A_WeaponReady
		Loop 
	Deselect:
		NULL A 0 A_Lower
		SHTX A 1 A_Lower
		Loop
	Select:
		NULL A 0 A_Raise
		SHTX G 1 A_Raise
		Loop
	Fire:
		NULL A 0 A_TakeInventory("BotAction1", 1)
		SHTX A 1 
		NULL A 0 A_PlaySound("mshotgun/fire", CHAN_WEAPON)
		NULL A 0 ACS_NamedExecuteWithResult("hvm_traceroffset", -8, 0)
		XHTG A 1 Bright A_FireBullets(3.5, 3.5, 7, 8, "BulletPuff", FBF_NORANDOM | FBF_USEAMMO)
		NULL A 0 A_GunFlash
		XHTG BCBA 1 Bright
		SHTX ABCDEFGHIJ 1
		SHTX K 2 A_PlaySound("mshotgun/pump", CHAN_WEAPON)
		NULL A 0 A_FireCustomMissile("ShellCasingSpawner", 0, 0, -20, -1)
		SHTX LKJIHGFEDCB 1
		SHTX A 9
		SHTX A 1 A_ReFire
		Goto ShotgunReady
	Flash:
		NULL A 1 Bright A_Light1
		NULL A 1 Bright A_Light2
		NULL A 1 Bright A_Light1
		NULL A 1 Bright A_Light0
		Stop
	Spawn:
		XSOT A -1
		Stop
	}
}

Actor "Marine Pistol" : Weapon
{
	Weapon.SlotNumber 2
	Obituary "%o had a bullet shot into %p face by %k's accurate pistol."

	Weapon.AmmoType "PistolClip"
	Weapon.AmmoUse 1
	
	Weapon.UpSound "mpistol/raise"
	Decal "Bullet"

	States
	{
	Ready:
		NULL A 0 A_JumpIfInventory("BotAction1", 1, "Fire")
		PKPI A 1 A_WeaponReady
		Loop
	Deselect:
		NULL A 0 A_Lower
		PKPI A 1 A_Lower
		Loop
	Select:
		NULL A 0 A_Raise
		PKPI A 1 A_Raise
		Loop
	Fire:
		NULL A 0 A_TakeInventory("BotAction1", 1)
		PKPI A 2 A_GunFlash
		NULL A 0 A_PlaySound("mpistol/fire", CHAN_WEAPON)
		NULL A 0 ACS_NamedExecuteWithResult("hvm_traceroffset", 0, 0)
		NULL A 0 A_FireBullets(2, 2, -1, 14, "BulletPuff", FBF_NORANDOM | FBF_USEAMMO)
		NULL A 0 A_FireCustomMissile("9mmCasingSpawner", 0, 0, 0, -6)
		PKPI BC 2
		PKPI DEDC 2
		PKPI B 2 A_ReFire
		Goto Ready
	Flash:
		PKPF A 2 Bright A_Light1
		Goto LightDone
	Spawn:
		PIST A -1
		Stop
	}
}

Actor "Marine Fists" : Weapon
{
	Weapon.SlotNumber 1
	Obituary "%k broke a few of %o's bones"
	
	Inventory.Icon "PUNGA0"
	
	+WEAPON.MELEEWEAPON
	+WEAPON.NOALERT
	
	Weapon.UpSound "fists/raise"
	AttackSound "fists/hit"

	States
	{
	Ready:
		PUNC ABCDEF 1
		Goto FistReady
	FistReady:
		NULL A 0 A_JumpIfInventory("BotAction1", 1, "Fire")
		PUNG A 1 A_WeaponReady
		Loop
	Deselect:
		PUNG A 1 A_Lower
		Loop
	Select:
		PUNC A 1
		NULL AA 0 A_Raise
		Loop
	Fire:
		NULL A 0 A_TakeInventory("BotAction1", 1)
		PUNG B 2
		PUNG C 2 A_PlaySound("fists/swing", CHAN_WEAPON)
		PUNG D 2
		PUNG E 2 A_CustomPunch(30, 1, 0, "PunchPuff")
		PUNG FGHI 1
		PUNG I 1
		NULL A 0 A_Refire
		Goto FistReady
	}
}
