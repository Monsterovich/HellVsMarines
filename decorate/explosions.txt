// Cool explosion by Monsterovich.

Actor BoomParticle : HvMCollidingEffect
{
	+DOOMBOUNCE

	Height 16
	Radius 8

	Speed 10
	BounceFactor 0.3
	
	RenderStyle Add
	Alpha 0.8
	Scale 0.75
	
	States
	{
	Spawn:
		MISL BBBBBBBB 1 Bright A_SpawnItemEx("BigFlameSpawner", 0, 0, 0, random(1,6), random(1,6), random(1,6))
		MISL CCCCCC 1 Bright A_SpawnItemEx("BigFlameSpawner", 0, 0, 0, random(1,6), random(1,6), random(1,6))
		MISL DD 1 Bright A_SpawnItemEx("BigFlameSpawner", 0, 0, 0, random(1,6), random(1,6), random(1,6))
		MISL DD 1 Bright
		Stop
	}
}

Actor SmallBoomParticle : BoomParticle
{
	Scale 0.25
	States
	{
	Spawn:
		MISL BBBBBBBB 1 Bright A_SpawnItemEx("SmallFlameSpawner", 0, 0, 0, random(1,6), random(1,6), random(1,6))
		MISL CCCCCC 1 Bright A_SpawnItemEx("SmallFlameSpawner", 0, 0, 0, random(1,6), random(1,6), random(1,6))
		MISL DD 1 Bright A_SpawnItemEx("SmallFlameSpawner", 0, 0, 0, random(1,6), random(1,6), random(1,6))
		MISL DD 1 Bright
		Stop
	}
} 

Actor BoomParticleSpawner : HvMEffect
{
	States
	{
	Spawn:
		NULL A 0
		NULL AAAAAA 0 A_SpawnItemEx("BoomParticle", 0, 0, 0, (0.1)*Random(50, 100), 0, (0.1)*Random(-20, 20), Random(0, 360), 128)
		NULL ABCDE 5 // Dynamic lights.
		Stop
	}
}

Actor SmallBoomParticleSpawner : HvMEffect
{
	States
	{
	Spawn:
		NULL A 0
		NULL AAAAA 0 A_SpawnItemEx("SmallBoomParticle", 0, 0, 0, (0.1)*Random(50, 100), 0, (0.1)*Random(-20, 20), Random(0, 360), 128)
		NULL ABCDE 5 // Dynamic lights.
		Stop
	}
}

Actor NadeSlag : HvMCollidingEffect // stolen from Beautiful Doom!!!!!!!!!!!11111
{
	-NOGRAVITY
	+DOOMBOUNCE
	+BOUNCEONACTORS

	Gravity 0.7
	BounceFactor 0.65
	Speed 11
	
	Scale 0.4
	RenderStyle Add

	States
	{
	Spawn:
    	P2FF A 0
		P2FF A 1 Bright A_Jump(2, "Death")
		Loop
	Death:
		P2FF A 1 Bright A_FadeOut(0.04)
		Loop
	}
}