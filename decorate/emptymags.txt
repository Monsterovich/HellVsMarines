Actor EmptyMag : HvMCollidingEffect
{
	-DONTSPLASH
	-NOGRAVITY
	+DOOMBOUNCE
	
	Radius 6
	Height 12
	Speed 6
	BounceFactor 0.5
}

ACTOR RifleEmptyMag : EmptyMag
{
	Scale 0.85
    SeeSound "emptymag/fall"
    States
    {
	Spawn:
	  ECLI ABCDEF 3  
	  Loop
    Death:
	  TNT1 A 0
      TNT1 A 0 A_Jump(128,3)
      ECLI A 1000
      ECLI A 5 A_FadeOut(0.01)
      Goto Death+3
      ECLI C 1000
	  ECLI C 5 A_FadeOut(0.01)
	  Goto Death+5
	  }
}

Actor PistolEmptyMag : RifleEmptyMag { Scale 0.7 }
